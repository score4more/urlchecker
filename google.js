import fs from 'fs-extra'
import PATH from 'path'
import {authenticate} from '@google-cloud/local-auth';
import {google} from 'googleapis';

const basePath = PATH.join(process.cwd(),'credentials');
const tokenPath = PATH.join(basePath,'token.json');
const credentialsPath = PATH.join(basePath,'google.json');
const scopes = [
	'https://www.googleapis.com/auth/spreadsheets'
]
export const googleApi = google

async function loadSavedCredentialsIfExist() {
	try {
		return google.auth.fromJSON(await fs.readJson(tokenPath));
	} catch (err) {
		return null;
	}
}

async function saveCredentials(client) {
	const keys = await fs.readJson(credentialsPath);
	const key = keys.installed || keys.web;
	const payload = JSON.stringify({
		type: 'authorized_user',
		client_id: key.client_id,
		client_secret: key.client_secret,
		refresh_token: client.credentials.refresh_token,
	});
	await fs.writeFile(tokenPath, payload);
}

export async function authorize() {
	let client = await loadSavedCredentialsIfExist();
	if (client) {
		return client;
	}
	client = await authenticate({
		scopes: scopes,
		keyfilePath: credentialsPath,
	});
	if (client.credentials) {
		await saveCredentials(client);
	}
	return client;
}

export async function getSheetConfig(){
	return await fs.readJson(PATH.join(basePath,'config.json'))
}
