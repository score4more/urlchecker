import axios from "axios";
import { measureTime } from 'measure-time';
import {authorize, getSheetConfig, googleApi} from './google.js';

const responseStrings = {
	SUCCESS: 'Success',
	NOT_FOUND: 'Not Found',
	FAIL: 'Failed'
}
const instance = axios.create({
	timeout: 3000,
	maxRedirects: 3,
	validateStatus: function (status) {
		return status >= 200 && status < 300; // default
	}
})

async function testUrl(url, retries = 0){
	if(url && !url.startsWith('#')){
		try {
			if(url.startsWith("http") || url.startsWith('https')){
				await instance.get(url)
				return responseStrings.SUCCESS
			}
			else {
				try {
					return await testUrl(`https://${url}`)
				}
				catch (e){
					return await testUrl(`http://${url}`)
				}
			}
		}
		catch (e){
			if(e.isAxiosError) {
				if(retries >= 3) return await testUrl(url,retries + 1)
				return responseStrings.NOT_FOUND
			}
			else throw e
		}
	}
	else return responseStrings.FAIL
}

async function init(){
	const auth = await authorize()
	const config = await getSheetConfig()
	const sheets = googleApi.sheets({version: 'v4', auth});
	const res = await sheets.spreadsheets.values.get({
		...config,
		majorDimension: 'ROWS'
	});
	const headers = res.data.values[0];
	const rows = res.data.values.splice(1);
	if (!rows || rows.length === 0) {
		console.log('No data found.');
		return;
	}
	const colIndexes = {
		website: headers.indexOf('website'),
		url: headers.indexOf('source1_url'),
	}
	let index = 1
	headers.push('website_status', 'source1_status')
	for(const row of rows){
		const getElapsed = measureTime()
		row.push(
			await testUrl(row[colIndexes.website]),
			await testUrl(row[colIndexes.url])
		)

		console.log(`Finished ${index}/${rows.length} - ${getTime(getElapsed())}`)
		index++
	}
	rows.unshift(headers)
	await sheets.spreadsheets.values.update({
		...config,
		valueInputOption:'USER_ENTERED',
		requestBody: {
			values: rows
		}
	});
	return rows.length
}
const getElapsed = measureTime()
init().then((rows) => {
	console.log(`${rows} rows were updated in ${getTime(getElapsed())}`)
})

function getTime(elapsed){
	const {seconds,milliseconds} = elapsed
	return seconds === 0 ? `${milliseconds}ms` : `${seconds}s`
}
