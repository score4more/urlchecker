## Prerequisites
1. Ensure [GIT](https://www.git-scm.com/downloads) is installed. Run ````git -v````. If you see a version number, you're all set
2. Ensure that [NodeJS](https://nodejs.org/en) is installed by opening a CMD and running ````node -v````. If you see a version number you're all set
3. Follow the setup for the [Google Sheets API](https://developers.google.com/sheets/api/quickstart/nodejs)
4. Do not forget to add your Google account, and any other that needs access to this application, as a test user under the ``OAuth Consent Screen`` section of the Google Cloud console
5. Download the credentials file as ````google.json```` and store it in the ````credentials```` directory
6. Under the ```credentials``` directory copy the file ```config.example.json``` to ```config.json``` and fill in the values between the double quotes
         - The ```spreadsheetId``` can be found in the URL of the Google sheet. It can be found in the following position of the URL:  ```https://docs.google.com/spreadsheets/d/{SPREDSHEET_ID}/edit```
         - The required range can be obtained by selecting the range and selecting `````View more cellactions -> Define named range````` from the context menu and copying the value of the second input. It is recommended to select the entire worksheet

## Installation
1. Clone this repository ````git clone https://gitlab.com/score4more/urlchecker.git````
2. Go into the cloned folder ````cd urlChecker````
3. Install dependencies ````npm i````
4. Start the application ````npm start````.
   - Before starting, see the Usage section!

## Usage
Select a range in any worksheet you wish to check the urls for. When selecting the range, make sure that it does not include any letters (use 1:15 instead og A1:B15)
